﻿using PersonalManager.Data_Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalManager
{
    /// <summary>
    /// Логика взаимодействия для Personal.xaml
    /// </summary>
    public partial class EmployeeInfo : Window
    {
        ConnectorDB db;
        MainWindow main1;
        public EmployeeInfo()
        {            
            InitializeComponent();

            db = new ConnectorDB();
            db.Users.Load();
            usersGrid.ItemsSource = db.Users.Local.ToBindingList();

            ClosePersonal.Click += (sender, e) => ExitPersonalWindow();
        }

        private void ExitPersonalWindow()
        {
            Close();
            main1 = new MainWindow();
            main1.ShowDialog();
        }
    }
}
