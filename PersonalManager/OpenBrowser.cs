﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PersonalManager
{
    class OpenBrowser
    {
        /// <summary>
        /// Список возможных браузеров.
        /// </summary>
        static List<string> browsers = new List<string>()
        {
            "chrome",
            "edge",            
            "firefox",
            "opera",            
        };

        /// <summary>
        /// Открывает браузер, если он установлен на ПК.
        /// </summary>
        /// <returns></returns>
        public static string OpenWebBrowser()
        {
            string br = null;
            try
            {
                br = GetBrowser(browsers);
                if (br == GetBrowser(browsers))
                {
                    if (br != null)
                    {
                        Process.GetProcessesByName(br);
                        Process.Start(br);
                        return br;
                    }
                    else
                        MessageBox.Show($"Возникли проблемыс загрузкой браузера!", "Внимание!", MessageBoxButton.OK);
                }
            }
            catch (Exception)
            {
                MessageBox.Show($"Браузер {br} не найден! Нажмите кнопку 'Browser' снова!", "Внимание!", MessageBoxButton.OK);
                browsers.RemoveAt(0);
                GetBrowser(browsers);
            }
            return default;
        }

        /// <summary>
        /// Перебор списка с браузерами.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static string GetBrowser(List<string> list)
        {
            foreach (var browser in list)
            {
                return browser;
            }
            return default;
        }
    }
}
