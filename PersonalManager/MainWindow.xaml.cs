﻿using PersonalManager.Data_Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PersonalManager
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ConnectorDB db;
        AddUser company = new AddUser();
        EmployeeInfo personal = new EmployeeInfo();

        public MainWindow()
        {
            InitializeComponent();
            db = new ConnectorDB();
            db.Users.Load();

            Profile.Click += (sender, e) => OpenWindow(company);
            Personal.Click += (sender, e) => OpenWindow(personal);
            Browser.Click += (sender, e) => OpenBrowser.OpenWebBrowser();
            Exit.Click += (sender, e) => Close();
        }

        /// <summary>
        /// Открывает нужное окно по нажатию соответствующей кнопки.
        /// </summary>
        /// <param name="title"></param>
        void OpenWindow(Window title)
        {
            title.Show();
            Close();
        }
    }
}
