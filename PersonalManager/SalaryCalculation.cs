﻿using PersonalManager.Data_Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PersonalManager
{
    class SalaryCalculation
    {
        /// <summary>
        /// Подсчёт зарплаты.
        /// </summary>
        /// <param name="actualR"></param>
        /// <param name="workDay"></param>
        /// <param name="workHour"></param>
        /// <returns></returns>
        public static decimal CalcualteOfTheSalary(decimal actualR, int workDay, int workHour)
        {
            decimal zarplata = actualR * workHour * workDay;
            return zarplata;
        }

        /// <summary>
        /// Подсчёт зарплаты, учитывая бонусы и количество лет работы в компании.
        /// </summary>
        /// <param name="groupTitle"></param>
        /// <param name="bonus"></param>
        /// <param name="yearIn"></param>
        /// <param name="maxVal"></param>
        /// <returns></returns>
        public static decimal ActualSalary(string groupTitle, decimal salary, decimal baseRate, string date, decimal bonus, DateTime yearIn, int maxVal)
        {
            yearIn = Convert.ToDateTime(date);
            DateTime yearNow = DateTime.Now;
            decimal yearResult = yearNow.Year - yearIn.Year;

            if (yearResult == 0)
            {
                return CalcualteOfTheSalary(baseRate, 22, 8);
            }
            if (groupTitle == groupTitle && yearResult >= 1 && yearResult <= maxVal || yearResult > maxVal)
            {
                decimal partSalary = 0;
                decimal salaryWithBonus = 0;

                for (int i = 1; i <= yearResult && i < maxVal; i++)
                {
                    partSalary = salary * bonus;
                    partSalary *= i;
                    salaryWithBonus = partSalary + salary;
                }
                return salaryWithBonus;
            }           
            else
                MessageBox.Show("Недопустимое значение для результата даты!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
            return default;
        }
    }
}
