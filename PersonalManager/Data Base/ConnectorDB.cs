﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalManager.Data_Base
{
    class ConnectorDB : DbContext
     {

        public ConnectorDB() : base("DefaultConnection")
        {
        }
        public DbSet<User> Users { get; set; }
    }
}
