﻿using PersonalManager.Data_Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PersonalManager
{
    /// <summary>
    /// Логика взаимодействия для Company.xaml
    /// </summary>
    public partial class AddUser : Window
    {
        ConnectorDB db;
        MainWindow window;

        User user = new User();

        public AddUser()
        {
            db = new ConnectorDB();
            InitializeComponent();

            BtnAddUser.Click += (sender, e) => AddNewUser();
            Exit.Click += (sender, e) => ExitWindow();
        }

        /// <summary>
        /// Кнопка добавления нового пользователя в БД.
        /// </summary>
        void AddNewUser()
        {
            try
            {
                user.Name = NameText.Text;
                user.Sername = SernameText.Text;
                user.Group = GroupText.Text;
                user.Date = Calendar.SelectedDate.ToString();             

                if (user.Group == "Employee")
                {
                    user.Rate = 200;
                    user.Bonus = 0.03m;
                    user.Salary = SalaryCalculation.CalcualteOfTheSalary(user.Rate, 22, 8);
                    user.SalaryWithBonus = SalaryCalculation.ActualSalary(user.Group, user.Salary, user.Rate, user.Date, user.Bonus, Convert.ToDateTime(user.Date), 10);
                }
                if (user.Group == "Manager")
                {
                    user.Rate = 170;
                    user.Bonus = 0.05m;
                    user.Salary = SalaryCalculation.CalcualteOfTheSalary(user.Rate, 22, 8);
                    user.SalaryWithBonus = SalaryCalculation.ActualSalary(user.Group, user.Salary, user.Rate, user.Date, user.Bonus, Convert.ToDateTime(user.Date), 6);
                }
                if (user.Group == "Saler")
                {
                    user.Rate = 150;
                    user.Bonus = 0.01m;
                    user.Salary = SalaryCalculation.CalcualteOfTheSalary(user.Rate, 22, 8);
                    user.SalaryWithBonus = SalaryCalculation.ActualSalary(user.Group, user.Salary, user.Rate, user.Date, user.Bonus, Convert.ToDateTime(user.Date), 35);
                }
                
                db.Users.Add(user);
                MessageBox.Show($"Пользователь{user.Name} {user.Sername} добавляется! Нажмите ОК!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                db.SaveChanges();
                MessageBox.Show($"Пользователь{user.Name} {user.Sername} добавляен!", "Готово!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (FormatException ex)
            {
                MessageBox.Show($"В одном из полей передаётся неправильный формат данных!{ex.StackTrace}", 
                                 "Неверный формат данных", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        /// <summary>
        /// Метод закрытия окна 'Personal' и открытие MainWindow.
        /// </summary>
        public void ExitWindow()
        {
            Close();
            window = new MainWindow();
            window.ShowDialog();
        }
    }
}
