﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonalManager
{
    public class User 
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Sername { get; set; }
        public string Date { get; set; }
        public string Group { get; set; }
        public decimal Rate { get; set; }
        public decimal Bonus { get; set; }
        public decimal Salary { get; set; }
        public decimal SalaryWithBonus { get; set; }
        

        public User()
        {

        }

        public User(string name, string sername)
        {
            Name = name;
            Sername = sername;
        }

        public User(string name, string sername, string date, string group, decimal rate, decimal salary, decimal bonus, decimal salaryWithBonus)
        {
            Name = name;
            Sername = sername;
            Date = date;
            Group = group;
            Rate = rate;
            Salary = salary;
            Bonus = bonus;
            SalaryWithBonus = salaryWithBonus;
        }
    }
}
